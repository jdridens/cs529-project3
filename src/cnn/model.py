import tflearn as tf
import tflearn.layers as tfl


def conv2d(xd, yd):
    """
    Tensorflow model that performs a 2d convolution over data.
    We used this site to help build our model:
    https://github.com/tflearn/tflearn/blob/master/examples/images/cn_cifar10.py

    Weights were determined from advice on the net.
    We opted out of preprocessing.
    """
    cn = tfl.input_data(shape=[None, xd, yd, 1], name='input')

    # Make a bunch of convolutional layers "deep"
    cn = tfl.conv_2d(cn, 64, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_2d(cn, 2)

    cn = tfl.conv_2d(cn, 128, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_2d(cn, 2)

    cn = tfl.conv_2d(cn, 256, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_2d(cn, 2)

    cn = tfl.conv_2d(cn, 512, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_2d(cn, 2)

    # Dropout used to prevent overfitting
    cn = tfl.fully_connected(cn, 1024, activation='relu')
    cn = tfl.dropout(cn, 0.5)

    # Perform the regression that'll return to us the probabilities a train/test
    # belongs to a particular class
    cn = tfl.fully_connected(cn, 10, activation='softmax')
    cn = tfl.regression(cn, optimizer='adam', learning_rate=0.001,
                        loss='categorical_crossentropy', )

    # Bundle this all up into a model
    model = tf.DNN(cn,
                   best_checkpoint_path="..\\..\\data\\checkpoints\\best\\",
                   best_val_accuracy=0.8)
    return model


def conv1d(input_size):
    """
    Largely a copy of the above, just changed the 2d layers to 1d layers.
    :param input_size:
    :return:
    """
    cn = tfl.input_data(shape=[None, input_size, 1], name='input')

    # Make a bunch of convolutional layers "deep"
    cn = tfl.conv_1d(cn, 32, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_1d(cn, 2)

    cn = tfl.conv_1d(cn, 64, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_1d(cn, 2)

    cn = tfl.conv_1d(cn, 128, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_1d(cn, 2)

    cn = tfl.conv_1d(cn, 256, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_1d(cn, 2)

    cn = tfl.conv_1d(cn, 512, 2, activation='relu', weights_init="Xavier")
    cn = tfl.max_pool_1d(cn, 2)

    # Dropout used to prevent overfitting
    cn = tfl.fully_connected(cn, 1024, activation='relu')
    cn = tfl.dropout(cn, 0.5)

    # Perform the regression that'll return to us the probabilities a train/test
    # belongs to a particular class
    cn = tfl.fully_connected(cn, 10, activation='softmax')
    cn = tfl.regression(cn, optimizer='adam', learning_rate=0.001,
                        loss='categorical_crossentropy', )

    # Bundle this all up into a model
    model = tf.DNN(cn,
                   best_checkpoint_path="..\\..\\data\\checkpoints\\best\\",
                   best_val_accuracy=0.8)
    return model
