import numpy as np
import tflearn as tf
import tensorflow as t
import src.cnn.model as m
import random as r
import h5py as h5
import sklearn.metrics as sm
import scipy.stats as st
import matplotlib.pyplot as plt
import src.data.genre as g
import src.data.confusion as cf
import os as os


def chunk(data, siz):
    """
    Similar to above, we break a 1d array into several chunks and return an
    array containing those chunks.
    """
    return [data[i:i + siz] for i in range(0, len(data), siz)]


def concat(xs):
    """
    Removes the outer list, returning the internals appended together.
    """
    return [i for x in xs for i in x]


"""
For each feature we collected we added a method that would perform the training.
"""


def train_mfcc1d(data): valtrain(data, [-1, 13, 1], m.conv1d(13), "mfcc1d")


def train_fft(data): valtrain(data, [-1, 1000, 1], m.conv1d(1000), 'fft')


def train_melspec(data): valtrain2d(data, [-1, 128, 128, 1],
                                    m.conv2d(128, 128), 'melspec')


def train_mfcc2d(data): valtrain2d(data, [-1, 128, 128, 1],
                                   m.conv2d(128, 128), 'mfcc2d')


def train_chroma(data): valtrain2d(data, [-1, 12, 128, 1],
                                   m.conv2d(12, 128), 'chroma')


def train_tonnetz(data): valtrain2d(data, [-1, 6, 128, 1],
                                    m.conv2d(6, 128), 'tonnetz')


def train_stft(data): valtrain2d(data, [-1, 12, 128, 1],
                                 m.conv2d(12, 128), 'stft')


def fulltrain(xdata, ydata, model, name):
    """
    fulltrain uses all of the data from the training. This was used to produce
    submissions to kaggle.
    """
    save_dir = "..\\..\\data\\cnn-saves\\"
    xtrain = np.array(xdata).reshape([-1, 128, 128, 1])
    ytrain = tf.data_utils.to_categorical(ydata, 10)
    model.fit(xtrain, ytrain,
              n_epoch=100,
              batch_size=96,
              run_id=name,
              show_metric=True,
              shuffle=True)
    model.save(save_dir + name + '.model')


def valtrain(data, xreshape, model, name):
    """
    valtrain uses 10% of the data to perform validation, which allowed us to
    produce the confusion matrices.
    """
    # Initialize confusion matrix
    cm = np.zeros((10, 10))

    # Initialize accuracy list
    acc = []

    # Train and compute confusion matrix for each fold
    for xtrain, ytrain, xval, yval in split_data(data, 0.1, xreshape):
        t.reset_default_graph()
        model = m.conv1d(1000)
        model.fit(xtrain, ytrain,
                  validation_set=[xval, yval],
                  n_epoch=100,
                  batch_size=96,
                  run_id=name,
                  show_metric=True,
                  shuffle=True)

        # Predict over val data
        ypred = model.predict(xval)
        preds = list(map(lambda q: np.argmax(q), ypred))

        # Decode the one hot encoding
        trues = list(map(lambda q: np.argmax(q), yval))

        # add confusion matrix
        cm += sm.confusion_matrix(trues, preds)

        # Get accuracy
        total = 0
        correct = 0
        for a, b in zip(trues, preds):
            total += 1
            if a == b:
                correct += 1
        acc.append(correct/total)

    # Print mean of accuracy
    print("Accuracy: {}".format(np.mean(acc)))

    # Plot confusion matrix
    np.set_printoptions(precision=2)
    plt.figure()
    cf.plot_confusion_matrix(cm.astype(int), classes=g.genre_lookup,
                             title='Confusion Matrix')
    plt.show()


def valtrain2d(data, xreshape, model, name):
    """
    valtrain2d uses 10% of the data to perform validation, which allowed us to
    produce the confusion matrices.

    The extra code in this function handles the fact that our 2d data has been
    chunked, and to report accuracy correctly requires us to manage that data
    during the shuffling and predicting areas.
    """
    # Initialize Confusion Matrix
    cm = np.zeros((10, 10))

    # Initialize accuracy list
    acc = []

    # Train and create confusion matrix over the k fold split
    for xtrain, ytrain, xval, yval in split_data2d(data, 0.1, xreshape):
        t.reset_default_graph()
        model = m.conv2d(128, 128)
        model.fit(xtrain, ytrain,
                  validation_set=[xval, yval],
                  n_epoch=10,
                  batch_size=96,
                  run_id=name,
                  show_metric=True,
                  shuffle=True)

        ps = []
        # Running on a GPU causes memory issues, so break up predication data
        # to prevent OOM error.
        for xchunk in chunk(xval, 100):
            # Predict over val data
            ypred = model.predict(xchunk)
            preds = list(map(lambda q: np.argmax(q), ypred))

            # Grab song data (should be grouped in 10s)
            pss = chunk(preds, 10)
            pss = list(map(lambda q: st.mode(q)[0][0], pss))

            # Append list to ps accumulator
            ps += pss

        # Decode the one hot encoding
        yval = list(map(lambda q: np.argmax(q), yval))

        # Grab song data (should be grouped in 10s)
        yval = chunk(yval, 10)
        trues = list(map(lambda q: q[0], yval))

        # add confusion matrix
        cm += sm.confusion_matrix(trues, ps)

        # Get accuracy
        total = 0
        correct = 0
        for a, b in zip(trues, ps):
            total += 1
            if a == b:
                correct += 1

        acc.append(correct/total)

    # Print mean of accuracy
    print("Accuracy: {}".format(np.mean(acc)))

    # Plot confusion matrix
    np.set_printoptions(precision=2)
    plt.figure()
    cf.plot_confusion_matrix(cm.astype(int), classes=g.genre_lookup,
                             title='Confusion Matrix')
    plt.show()


def split_data(data, pval, xreshape):
    """
    This method splits the data in training and validation sets, while also
    resizing the X data to fit the convultion network.
    """
    for i in range(0, 8):
        r.shuffle(data)
    siz = np.floor(len(data) * pval).astype(int)

    # Break the dataset into the folds
    datachunks = chunk(data, siz)
    k = []
    i = 0

    # Create combinations of training and validation sets
    for _ in range(0, 10):
        trainset = datachunks[i]
        valset = concat(datachunks[:i] + datachunks[(i+1):])
        xt, yt = zip(*iter(trainset))
        tx, ty = zip(*iter(valset))

        # Format the data for the CNN and add it to our lists of folds
        k.append((np.array(xt).reshape(xreshape),
                  tf.data_utils.to_categorical(yt, 10),
                  np.array(tx).reshape(xreshape),
                  tf.data_utils.to_categorical(ty, 10)))
        i += 1

    return k


def split_data2d(data, pval, xreshape):
    """
    This method splits the data in training and validation sets, while also
    resizing the X data to fit the convultion network.

    This method also groups the data into 10s (each song training sample was
    grouped into 10s, we want to shuffle the songs, not their individual
    pieces)
    """
    xs, ys = zip(*data)

    # Group data by song
    xss = list(zip(*(iter(xs),) * 10))
    yss = list(map(lambda l: l[0], list(zip(*(iter(ys),) * 10))))
    xys = list(zip(xss, yss))

    # shuffle it up
    for i in range(0, 8):
        r.shuffle(xys)
    siz = np.floor(len(xys) * pval).astype(int)

    # create the folds
    datachunks = chunk(xys, siz)
    k = []
    i = 0

    # Create combinations of training and validation
    for _ in range(0, 10):
        trainset = datachunks[i]
        valset = concat(datachunks[:i] + datachunks[(i+1):])
        xt, yt = zip(*iter(trainset))
        tx, ty = zip(*iter(valset))

        # Format data for the CNN
        k.append((np.array(xt).reshape(xreshape),
                  tf.data_utils.to_categorical(np.repeat(yt, 10), 10),
                  np.array(tx).reshape(xreshape),
                  tf.data_utils.to_categorical(np.repeat(ty, 10), 10)))
        i += 1

    return k


if __name__ == "__main__":
    # Load feature file
    h5f = h5.File("..\\..\\data\\data.hdf5", 'r')

    xs = h5f['training']['mfcc2d']['x']
    ys = h5f['training']['mfcc2d']['y']

    # valtrain the data
    train_mfcc2d(list(zip(xs, ys)))

    # close the feature file
    h5f.close()
