# Compute features, this file is script, so just import it
import src.data.features

# Get feature file
import h5py as h5

feats = h5.File("..\\data\\data.hdf5", 'r')

# XGBModel - Cross Val MFCC
import src.xgb.classifier as xgb

xgb.cross_validate_1d()

# CNN Model - Cross Val MFCC
import src.cnn.classifier as cnn

xs = feats['training']['mfcc1d']['x']
ys = feats['training']['mfcc1d']['y']
cnn.train_mfcc1d(list(zip(xs, ys)))

feats.close()
