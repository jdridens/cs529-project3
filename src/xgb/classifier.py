import xgboost as xgb
import numpy as np
import h5py as h5
import src.data.genre as g
import random as r
import sklearn.metrics as sm
import matplotlib.pyplot as plt
import scipy.stats as st
import src.data.confusion as cf


def chunk(data, siz):
    """
    Similar to above, we break a 1d array into several chunks and return an
    array containing those chunks.
    """
    return [data[i:i + siz] for i in range(0, len(data), siz)]


def concat(xs):
    """
    Removes the outer list, returning the internals appended together.
    """
    return [i for x in xs for i in x]


def cross_validate_2d():
    # These params were found through a grid search that optimizes training over
    # mfcc's.
    params = {
        'gamma': 0.0,
        'max_depth': 9,
        'reg_alpha': 0.3,
        'num_class': 10,
        'reg_lambda': 0.2,
        'learning_rate': 0.1,
        'objective': 'multi:softmax',
        'subsample': 0.9,
        'colsample_bytree': 0.9,
        'min_child_weight': 3
    }

    # Load feature file
    h5f = h5.File("..\\..\\data\\data.hdf5", 'r')

    # Handle the chunked up 2d data
    xs = np.array(h5f['training']['chroma']['x'])
    ys = h5f['training']['chroma']['y']
    xs = list(map(lambda q: np.mean(q.T, axis=0), xs))

    # group into groups of 10
    xss = list(zip(*(iter(xs),) * 10))

    # take every 10th label
    yss = list(map(lambda l: l[0], list(zip(*(iter(ys),) * 10))))
    xyss = list(zip(xss, yss))

    acc = []
    cm = np.zeros((10, 10))
    # Finally get back useable data for training and testing
    for x, y, xt, yt in split_data(xyss, 0.1):

        # Create training and testing matrices
        dtrain = xgb.DMatrix(data=concat(x), label=np.repeat(y, 10))
        dtest = xgb.DMatrix(concat(xt))

        # Train the tree
        bst = xgb.train(params, dtrain, num_boost_round=2000)

        # Create predictions
        preds = bst.predict(dtest).astype(int)

        # Vote on the predication for every group of 10 using their mode
        ps = chunk(preds, 10)
        ps = list(map(lambda q: st.mode(q)[0][0], ps))

        # add confusion matrix
        cm += sm.confusion_matrix(yt, ps)

        # Get accuracy
        total = 0
        correct = 0
        for a, b in zip(yt, ps):
            total += 1
            if a == b:
                correct += 1

        acc.append(correct/total)

    print("Accuracy: {}".format(np.mean(acc)))
    np.set_printoptions(precision=2)
    plt.figure()
    cf.plot_confusion_matrix(cm.astype(int), classes=g.genre_lookup,
                             title='Confusion Matrix')
    plt.show()


def cross_validate_1d():
    # These params were found through a grid search that optimizes training over
    # mfcc's.
    params = {
        'colsample_bytree': 0.5,
        'subsample': 0.9,
        'min_child_weight': 1,
        'learning_rate': 0.001,
        'gamma': 0.5,
        'reg_alpha': 1,
        'max_depth': 9,
        'objective': 'multi:softmax',
        'num_class': 10
    }

    # Load feature file
    h5f = h5.File("..\\..\\data\\data.hdf5", 'r')

    # Grab the data we want to play with
    xs = h5f['training']['mfcc1d']['x']
    ys = h5f['training']['mfcc1d']['y']
    xys = list(zip(xs, ys))

    acc = []
    cm = np.zeros((10, 10))
    # 10-fold xvalidation
    for xtrain, ytrain, xtest, ytest in split_data(xys, 0.1):

        dtrain = xgb.DMatrix(data=xtrain, label=ytrain)
        dtest = xgb.DMatrix(xtest)
        bst = xgb.train(params, dtrain, num_boost_round=2000)
        preds = bst.predict(dtest).astype(int)

        # add confusion matrix
        cm += sm.confusion_matrix(ytest, preds)

        # Get accuracy
        total = 0
        correct = 0
        for a, b in zip(ytest, preds):
            total += 1
            if a == b:
                correct += 1

        acc.append(correct/total)

    print("Accuracy: {}".format(np.mean(acc)))

    # plot confusion matrix
    np.set_printoptions(precision=2)
    plt.figure()
    cf.plot_confusion_matrix(cm.astype(int), classes=g.genre_lookup,
                             title='Confusion Matrix')
    plt.show()


def split_data(data, pval):
    """
    This method splits the data in training and validation sets, while also
    resizing the X data to fit the convultion network.
    """
    for i in range(0, 8):
        r.shuffle(data)
    siz = np.floor(len(data) * pval).astype(int)

    # Create the folds
    datachunks = chunk(data, siz)
    k = []
    i = 0

    # Create training and testing sets
    for _ in range(0, 10):
        trainset = datachunks[i]
        valset = concat(datachunks[:i] + datachunks[(i+1):])
        xt, yt = zip(*iter(trainset))
        tx, ty = zip(*iter(valset))
        k.append((np.array(xt), np.array(yt), np.array(tx), np.array(ty)))
        i += 1

    return k


if __name__ == "__main__":
    cross_validate_1d()
