import librosa as lr
import librosa.feature as lf
import h5py as h5
import os as os
import numpy as np
import scipy.fftpack as sci
import src.data.genre as g


# Helper Functions
##################


def chunk2d(data, siz):
    """
    We use this function to break 2d matrices into chunks of a specific size.
    This function then returns an array of those chunks
    """
    return np.array([data[:, i:i + siz] for i in range(0, len(data.T), siz)])


def chunk1d(data, siz):
    """
    Similar to above, we break a 1d array into several chunks and return an
    array containing those chunks.
    """
    return np.array([data[i:i + siz] for i in range(0, len(data), siz)])


# Test Feature extraction
#########################

# Get list of filepaths for testing files
testfiles = []
for root, _, files in os.walk("..\\..\\data\\rename\\"):
    for file in files:
        testfiles.append(os.path.join(root, file))

# Initialize lists to hold the features of the test files
mf1d_x = []
fft_x = []
mfcc2d_x = []
tonnetz_x = []
chroma_x = []
melspec_x = []

# Initialize lists to hold the names of the test files
mf1d_y = []
fft_y = []
mfcc2d_y = []
tonnetz_y = []
chroma_y = []
melspec_y = []

# For each test file compute the features we want to use
for test in testfiles:
    y, sr = lr.load(test, mono=True)

    # Get file name of the test
    label = os.path.basename(test)

    # Problem 1A
    fft_x.append(np.abs(sci.fft(y)[:1000]))
    fft_y.append(label)

    # Problem 1B
    mfcc_raw1d = lf.mfcc(y=y, sr=sr, n_mfcc=13)
    mf1d_x.append(np.mean(mfcc_raw1d.T, axis=0))
    mf1d_y.append(label)

    # Problem 1C
    stft_raw = lr.stft(y=y)
    mfcc_raw = lf.mfcc(y=y, sr=sr, n_mfcc=128)[:, :1280]
    melspec_raw = lf.melspectrogram(y=y, sr=sr)[:, :1280]
    chroma_raw = lf.chroma_stft(S=stft_raw, sr=sr)[:, :1280]
    tonnetz_raw = lf.tonnetz(y=lr.effects.harmonic(y), sr=sr)[:, :1280]

    # We want to break the data into chunks for better training. We also get
    # more training data doing this, which is nice.
    for c in chunk2d(mfcc_raw, 128):
        mfcc2d_x.append(c)
        mfcc2d_y.append(label)
    for c in chunk2d(tonnetz_raw, 128):
        tonnetz_x.append(c)
        tonnetz_y.append(label)
    for c in chunk2d(chroma_raw, 128):
        chroma_x.append(c)
        chroma_y.append(label)
    for c in chunk2d(melspec_raw, 128):
        melspec_x.append(c)
        melspec_y.append(label)

# Store testing data in file
############################

# We're using hdf5 to store the data, so we create a file to store everything
featfile = h5.File("..\\..\\data\\data.hdf5", "w")

# We create a group to hold the testing data
testing = featfile.create_group("testing")

# We create additional groups inside testing to hold each feature.
tonnetz_file = testing.create_group("tonnetz")
melspec_file = testing.create_group("melspec")
mfcc1d_file = testing.create_group("mfcc1d")
mfcc2d_file = testing.create_group("mfcc2d")
chroma_file = testing.create_group("chroma")
fft_file = testing.create_group("fft")

# Finally we add X data for each feature
tonnetz_file.create_dataset("x", data=np.array(tonnetz_x))
melspec_file.create_dataset("x", data=np.array(melspec_x))
mfcc1d_file.create_dataset("x", data=np.array(mf1d_x))
mfcc2d_file.create_dataset("x", data=np.array(mfcc2d_x))
chroma_file.create_dataset("x", data=np.array(chroma_x))
fft_file.create_dataset("x", data=np.array(fft_x))

# Finally we add Y data for each feature
tonnetz_file.create_dataset("y", data=np.array(tonnetz_y).astype('S'))
melspec_file.create_dataset("y", data=np.array(melspec_y).astype('S'))
mfcc1d_file.create_dataset("y", data=np.array(mf1d_y).astype('S'))
mfcc2d_file.create_dataset("y", data=np.array(mfcc2d_y).astype('S'))
chroma_file.create_dataset("y", data=np.array(chroma_y).astype('S'))
fft_file.create_dataset("y", data=np.array(fft_y).astype('S'))


# Training Feature Extraction
# ###########################

# This is largely a repeat from above, but over the training data

# Grab the training files
trainfiles = []
for root, _, files in os.walk("..\\..\\data\\genres\\"):
    for file in files:
        trainfiles.append(os.path.join(root, file))

# Initialize X and Y data arrays
mf1d_x = []
fft_x = []
mfcc2d_x = []
tonnetz_x = []
chroma_x = []
melspec_x = []

mf1d_y = []
fft_y = []
mfcc2d_y = []
tonnetz_y = []
chroma_y = []
melspec_y = []

# Get features for each training file
for train in trainfiles:
    y, sr = lr.load(train, mono=True)

    # Get label (genre) for the training file
    genre = g.from_genre(os.path.basename(train).split('.')[0])

    # Problem 1A
    fft_x.append(np.abs(sci.fft(y)[:1000]))
    fft_y.append(genre)

    # Problem 1B
    mfcc_raw1d = lf.mfcc(y=y, sr=sr, n_mfcc=13)
    mf1d_x.append(np.mean(mfcc_raw1d.T, axis=0))
    mf1d_y.append(genre)

    # Problem 1C
    stft_raw = lr.stft(y=y)
    mfcc_raw = lf.mfcc(y=y, sr=sr, n_mfcc=128)[:, :1280]
    melspec_raw = lf.melspectrogram(y=y, sr=sr)[:, :1280]
    chroma_raw = lf.chroma_stft(S=stft_raw, sr=sr)[:, :1280]
    tonnetz_raw = lf.tonnetz(y=lr.effects.harmonic(y), sr=sr)[:, :1280]

    # Chunk 2d data for nice benefits
    for c in chunk2d(mfcc_raw, 128):
        mfcc2d_x.append(c)
        mfcc2d_y.append(genre)
    for c in chunk2d(tonnetz_raw, 128):
        tonnetz_x.append(c)
        tonnetz_y.append(genre)
    for c in chunk2d(chroma_raw, 128):
        chroma_x.append(c)
        chroma_y.append(genre)
    for c in chunk2d(melspec_raw, 128):
        melspec_x.append(c)
        melspec_y.append(genre)

# Store training data in file
#############################

# Create training group
training = featfile.create_group("training")

# Create feature groups
tonnetz_file = training.create_group("tonnetz")
melspec_file = training.create_group("melspec")
mfcc1d_file = training.create_group("mfcc1d")
mfcc2d_file = training.create_group("mfcc2d")
chroma_file = training.create_group("chroma")
fft_file = training.create_group("fft")

# Add X data for the features
tonnetz_file.create_dataset("x", data=np.array(tonnetz_x))
melspec_file.create_dataset("x", data=np.array(melspec_x))
mfcc1d_file.create_dataset("x", data=np.array(mf1d_x))
mfcc2d_file.create_dataset("x", data=np.array(mfcc2d_x))
chroma_file.create_dataset("x", data=np.array(chroma_x))
fft_file.create_dataset("x", data=np.array(fft_x))

# Add Y data for the features
tonnetz_file.create_dataset("y", data=np.array(tonnetz_y))
melspec_file.create_dataset("y", data=np.array(melspec_y))
mfcc1d_file.create_dataset("y", data=np.array(mf1d_y))
mfcc2d_file.create_dataset("y", data=np.array(mfcc2d_y))
chroma_file.create_dataset("y", data=np.array(chroma_y))
fft_file.create_dataset("y", data=np.array(fft_y))

# Close the hdf5 file
featfile.close()
