"""
Simple script that we use to change genre strings into ints and back.
This is needed due to training labels needing to be encoded to ints.
"""


genre_lookup = ('BLUES', 'CLASSICAL', 'COUNTRY', 'DISCO', 'HIPHOP', 'JAZZ',
                'METAL', 'POP', 'REGGAE', 'ROCK')


def from_genre(genrestr):
    return genre_lookup.index(genrestr.upper())


def to_genre(genreint):
    return genre_lookup[genreint].lower()
