# cs529-project3
Music genre classification using XGBoost and Convolutional neural networks with
the GTZAN data set.

## Requirements

**Python 3.5** - We did not test any other version of python.

If you're on windows your best bet is to install Anaconda: https://www.anaconda.com/download/

This comes with numpy, scipy, and tons of other pretty good libraries. Otherwise
you'll need to install the required libraries for the pip install statement
below. 

Afterwards you'll need to 
`pip install tensorflow xgboost h5py librosa matplotlib`

## Using this tool

The program assumes this structure:

* project3/
  * data/
    * genres/
    * rename/
  * src/
 
**The training data is not included** Follow the information below
  
 The training data needs to exist in the data folder under the project root.
 Afterwards you can run 'src/data/features.py' to extract the data to a hdf5
 file. This process takes a long while.
 
 Once you have the feature file you can then run the CNN or the XGB tree. To do
 this you'll need to edit the corresponding 'classifier.py' code to pull 
 whatever data you want from the hdf5 file, and to run the training over it.
 
 Running the training will create a model, and produce a confusion matrix.
 
 Depending on the model you train this can take several minutes.
 
 In both 'classifier.py' under the `name == '__main__'` bit you'll find an
 example of what you need to do to train a model. For the CNN you'll also need
 to change the model that is used for crossvalidation to the dimensions of the
 data that is being trained. These dimension are as so:
 
 * chroma: 12x128
 * tonnetz: 6x128
 * mfcc1d: 13
 * mfcc2d: 128x128
 * fft: 1000
 
**One last thing of note:** CNN provided us the best accuracy, but to achieve
that accuracy you'll need to use the mfcc2d data set, and run the training for
65 epochs. This took us 5 hours an an i7 7800k, and around 2 hours on a GTX970.
For that reason we did not default our model to running that ammount of epochs,
instead we train for 10 epochs, which delivers a very subpar accuracy.

So feel free to add those epochs in, but do so carefully as tensorflow will use
all the resources you throw at it.

If feature extraction takes to long you can download the file from here:
https://drive.google.com/open?id=1Dbjz__b0ppHQ5hRtpRp7r9SblAN3pXzi

Just place the data.hdf5 file into the /data folder.

## HDF5 Data Usage

Each of these have an 'x' entry and a 'y' entry. You can pull the data out of
the file by navigating through the file appropriately. Essentially HDF5 is a
sort of file system, so navigating it behaves a lot like navigating data through
a file explorer.

The structure of the data set is as so:

* training
    * chroma
        * x
        * y
    * tonnetz
        * x
        * y
    * mfcc1d
        * x
        * y
    * mfcc2d
        * x
        * y
    * fft
        * x
        * y
* testing
    * chroma
        * x
        * y
    * tonnetz
        * x
        * y
    * mfcc1d
        * x
        * y
    * mfcc2d
        * x
        * y
    * fft
        * x
        * y
